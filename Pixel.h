#ifndef pixel
#define pixel

#include "HelperFunctions.h"
#include "Color.h"

class Pixel
{
public:
	Pixel(int r,int g,int b,int i)
	{
		this->color = new Color(r,g,b);
		this->index = i;
	}
	int R()
	{
		return this->color->R();
	}
	int G()
	{
		return this->color->G();
	}
	int B()
	{
		return this->color->B();
	}
	int getIndex()
	{
		return this->index;
	}
	Color* getColor()
	{
		return  this->color;
	}
private:
	int index;
	Color * color;
};

#endif
