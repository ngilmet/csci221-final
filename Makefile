all: FinalRunnable clean

FinalMain.o: FinalMain.cpp HelperFunctions.h BitmapReader.h AveragePixels.h
	g++ -Wall -ansi -g -O2 -c FinalMain.cpp -std=c++0x -pthread

FinalRunnable: FinalMain.o
	g++ -Wall -ansi -g -O2 -o FinalRunnable FinalMain.o -std=c++0x -pthread

clean:
	rm  -f *.o
