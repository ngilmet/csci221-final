#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

#include "BitmapReader.h"
#include "HelperFunctions.h"
#include "AveragePixels.h"
#include "Pixel.h"

using namespace std;


/**
 * The main method takes in 2 arguments, one of which being the input
 * photo, and alters the pixel values according to the mean R, G, and B
 * values.
 * 
 * @param int argc
 * @param int *argv[]
 * @return the distance
 */
int main(int argc, char *argv[]) {
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bhd;

	ifstream fp1 (argv[1], ios::in|ios::binary);
	if (!fp1.is_open()){
		cout << "Usage: " << argv[0] << " <input_filename> <output_filename>"<< endl;
		return 1;
	}

	int  success = 0;
	success = bfh.ReadBmpFileHeader(fp1) ;
	if (!success)
	{
		/* Couldn't read the file header - return NULL... */
		fp1.close();
		return -1;
	}
	
	success = bhd.ReadBmpInfoHeader(fp1);

	if (bfh.GETbfType() != BF_TYPE)  /* Check for BM reversed, ie MB... */
	{
		cout << "ID is: " <<  bfh.GETbfType() << " Should have been" << 'M'*256+'B';
		cout <<  bfh.GETbfType()/256 << " " <<  bfh.GETbfType()%256 << endl;
		/* Not a bitmap file - return NULL... */
		fp1.close();
		return 1;
	}


	cout << "Image data Size: " << bfh.GETbfSize() << endl;

	/* Image attributes are collected*/
    int imageWidth = bhd.GETbiWS();
	int imageHeight = bhd.GETbiHS();
	int totalPixels = imageWidth*imageHeight;

	cout << "Image Width Size:  " << imageWidth << endl;
	cout << "Image Height Size:  " << imageHeight << endl;
	cout<< "Bitcount: " << bhd.GETbiBitCount() << endl;

	vector<Pixel*> image;
	image.resize(totalPixels);
	static bool colorUsed[256][256][256];
	for(int a = 0 ; a < 256 ; a++)
		for(int b = 0 ; b < 256 ; b++)
			for(int c = 0 ; c < 256 ; c++)
				colorUsed[a][b][c] = false;


	int r = 0, g = 0, b = 0;

	/* 
		Loops through the pixels of the input image, and creates pixel object
		based upon them
	*/
    for (int i=0 ; i < totalPixels ; i++)
	{
		fp1.read ((char*)&b, 1);
		fp1.read ((char*)&g, 1);
		fp1.read ((char*)&r, 1);
		image[i] = new Pixel(r,g,b,i);
	}
	fp1.close ();

	int decimal = 0;

	AveragePixels averageColors (20);
	averageColors.image = image;

	/**
       * This loop calculates the distance between each RGB value
       * based upon each pixel object within the input image.
       * */
	clock_t begin = clock();
	clock_t end;
	for (int i = 0 ; i < totalPixels ; i++)
	{
		r = (image[i])->R();
		g = (image[i])->G();
		b = (image[i])->B();

		if(!colorUsed[r][g][b])
		{
			averageColors.addPixel(r,g,b);
			colorUsed[r][g][b] = true;
		}
		
		if ( (int)(((double)i/totalPixels)*100) > decimal )
		{
			end = clock();
			decimal = (int)(((double)i/totalPixels)*100);
			cout << decimal << "\t percent done | took " << double(end-begin)/CLOCKS_PER_SEC << " seconds" <<endl;
			begin = clock();
		}
	}

	ofstream fp2 (argv[2], ios::out|ios::binary);
	if (!fp2.is_open()){
		cout << "Usage: " << argv[0] << " <input_filename> <output_filename>"<< endl;
		return 1;
	}
	else
	{
		cout << "out file open" << endl;
	}

	bfh.WriteBmpFileHeader(fp2);
	bhd.WriteBmpInfoHeader(fp2);

	averageColors.printColors();
	Color* temp;
	int* tempint = new int;
    /**
       * This loop assigns the new RGB values to the output image utilizing 
       * properties of the bitmap reader.
       * */
	for(int i = 0 ; i < totalPixels ; i++)
	{
		if(!fp2.is_open())
		{
			cout << "THATS THE ISSUE" << endl;
			break;
		}

		temp = averageColors.getClosestColor(image[i]->R(),image[i]->G(),image[i]->B());
		*tempint = temp->B();
		fp2.write((char*) tempint,sizeof(char));
		*tempint = temp->G();
		fp2.write((char*) tempint,sizeof(char));
		*tempint = temp->R();
		fp2.write((char*) tempint,sizeof(char));
	}

	fp2.close ();
	return 0;
}