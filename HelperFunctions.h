#ifndef functions
#define functions

#include <cmath>

class Helper
{
public:
	static double calculateDistance(int x1,int y1,int z1,int x2,int y2,int z2);
};

double Helper::calculateDistance(int x1,int y1,int z1,int x2,int y2,int z2)
{
	int X = x1-x2,Y = y1-y2,Z = z1-z2;
	X = pow(X,2);
	Y = pow(Y,2);
	Z = pow(Z,2);
	return sqrt( (double) 1.0*(X+Y+Z));
}

#endif
