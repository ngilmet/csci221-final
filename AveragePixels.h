#ifndef advPixels
#define advPixels

#include <vector>
#include <set>
#include <thread>
#include "Color.h"
#include "Pixel.h"

class advColor
{
public:
	advColor(int r,int g,int b)
	{
		color = new Color(r,g,b);
	}
	Color* color;
	set<int> ownedPixels = {};
};

class AveragePixels
{
public:
	double avgDist;
	AveragePixels(int num);
	void addPixel(int r,int g,int b);
	void printColors();
	Color* getClosestColor(int r,int g,int b);
	bool containsColor(Color*);
	int numColors;
	vector<Pixel*> image;
	vector<advColor> colors;
	void calculationHelper(advColor col);
	double doCalculations(vector<advColor> colorVector);
};

void theMethod(AveragePixels that,vector<advColor> vec,advColor col,int i)
{
	double tempDist = that.doCalculations(vec);
	if(tempDist < that.avgDist)
	{
		that.colors.erase(vec.begin() + i);
		that.colors.push_back(col);
		that.avgDist = tempDist;
	}
}

AveragePixels::AveragePixels(int num)
{
	this->numColors = num;
}

double AveragePixels::doCalculations(vector<advColor> colorVector)
{
	int r,g,b,tempCount;
	double temp1,temp2,distance;
	long double tempDist = 0.0;
	for(int i1 = 0 ; i1 < image.size() ; i1++)
	{
		r = image[i1]->R();
		g = image[i1]->G();
		b = image[i1]->B();
		temp1 = 99999999.0;
		for(int i2 = 0 ; i2 < colorVector.size() ; i2++)
		{
			temp2 = Helper::calculateDistance(r,g,b,colorVector[i2].color->R(),colorVector[i2].color->G(),colorVector[i2].color->B());
			if(temp2 < temp1)
			{
				temp1 = temp2;
				tempCount = i2;
			}
		}
		colorVector[tempCount].ownedPixels.emplace(tempCount);
		tempDist += temp1;
	}
	distance = tempDist / image.size();
	return distance;
}

bool AveragePixels::containsColor(Color* color)
{
	for(vector<advColor>::reverse_iterator it = this->colors.rbegin() ; it != this->colors.rend() ; ++it )
	{
		if((*it).color->isSameColor(color)) return true;
	}
	return false;
}

void AveragePixels::addPixel(int r,int g,int b)
{
	advColor* color = new advColor(r,g,b);
	if(this->colors.size() < numColors) 
	{
		this->colors.push_back(*color);
	}
	else
	{
		vector<thread> threads;
		threads.resize(colors.size());
		for(int i = 0 ; i < colors.size() ; i++)
		{
			vector<advColor> temp = colors;
			temp[i] = *color;
			threads[i] = thread(theMethod,*this,temp,*(color),i);
		}
		for(int i = 0 ; i < threads.size() ; i++)
		{
			threads[i].join();
		}
	}
}

void AveragePixels::printColors()
{
	int i = 1;
	for(vector<advColor>::iterator it = colors.begin() ; it != colors.end() ; ++it )
	{
		cout << "Color " << i << endl;
		cout << "\tR :\t" << (*it).color->R() << endl;
		cout << "\tG :\t" << (*it).color->G() << endl;
		cout << "\tB :\t" << (*it).color->B() << endl;

		i++;
	}
}

Color* AveragePixels::getClosestColor(int r,int g,int b)
{
	double dist = numeric_limits<double>::max(),t;
	Color* temp = colors[0].color;
	for(vector<advColor>::iterator it = colors.begin() ; it != colors.end() ; ++it )
	{
		t = Helper::calculateDistance(r,g,b,(*it).color->R(),(*it).color->G(),(*it).color->B());
		if(t < dist)
		{
			dist = t;
			temp = (*it).color;
		}
	}
	return temp;
}
#endif