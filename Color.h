#ifndef colorclass
#define colorclass

class Color
{
public:
	Color(int r,int g,int b)
	{
		this->r = r;
		this->g = g;
		this->b = b;
	}
	int R()
	{
		return this->r;
	}
	int G()
	{
		return this->g;
	}
	int B()
	{
		return this->b;
	}
	void R(int a)
	{
		this->r = a;
	}
	void G(int a)
	{
		this->g = a;
	}
	void B(int a)
	{
		this->b = a;
	}
	bool isSameColor(Color* toCompare)
	{
		return (toCompare->R() == this->r && 
			toCompare->B() == this->b &&
			toCompare->G() == this->g);
	}
	double dist = -1;
private:
	int r,g,b;
};

#endif